//
//  EPCoreDataService.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/30/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPACoreDataService.h"
#import <CoreData/CoreData.h>
#import "EPAConstants.h"
@interface EPACoreDataService()

@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation EPACoreDataService

#pragma mark - init methods
+ (EPACoreDataService *)sharedService {
    static EPACoreDataService *service = nil;
    static dispatch_once_t once_token = 0;
    
    dispatch_once(&once_token, ^{
        service = [EPACoreDataService new];
    });
    
    return service;
}

- (void)setupCoreData {
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:EPAxcDataModelTitle withExtension:EPAExtencionForDataModel];
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:EPADataBaseTitle];
    NSError *error = nil;
    self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [self.managedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
}

#pragma mark - other methods
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveMedia:(EPAMediaDataModel *)media {
    
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:[NSEntityDescription entityForName:EPASavedMediaEntityName
                                   inManagedObjectContext:self.managedObjectContext]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:EPASortDescriptorKey
                                                                   ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *Fetcherror;
    NSMutableArray *mutableFetchResults = [[self.managedObjectContext
                                            executeFetchRequest:request error:&Fetcherror] mutableCopy];
    
    if (![[mutableFetchResults valueForKey:EPASortDescriptorKey]
          containsObject:media.mediaId]) {
        
        EPASavedMedia *mediaObject = [NSEntityDescription insertNewObjectForEntityForName:EPASavedMediaEntityName inManagedObjectContext:self.managedObjectContext];
        mediaObject.userName = media.userName;
        mediaObject.profileImage = [NSData dataWithContentsOfURL:media.profileImageURL];
        mediaObject.thumbnailImage = [NSData dataWithContentsOfURL:media.thumbnailImageURL];
        mediaObject.mediaId = media.mediaId;
        mediaObject.image = [NSData dataWithContentsOfURL:media.imageURL];
        mediaObject.video = [NSData dataWithContentsOfURL:media.videoURL];
        if (media.videoIndicator) {
            mediaObject.videoIndicator = @1;
        } else {
            mediaObject.videoIndicator = @0;
        }
        [mediaObject.managedObjectContext save:nil];
    }
}

- (void)deleteMedia:(EPASavedMedia *)media {
    [self.managedObjectContext deleteObject:media];
    [self.managedObjectContext save:nil];
}

@end
