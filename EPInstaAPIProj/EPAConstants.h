//
//  EPAConstants.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/7/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface EPAConstants : NSObject

extern NSString * const EPAInstagramApplicationAccessToken;
extern NSString * const EPADocumentDirectory;
extern NSString * const EPACollectionCellIdentifier;
extern NSString * const EPASavedMediaEntityName;
extern NSString * const EPAKeyForDescriptor;
extern NSString * const EPAFetchedResultControllerCacheName;
extern NSString * const EPADetailTableCellIdentifier;
extern NSString * const EPAWebPageForInternetCnnection;
extern NSString * const EPAInstagramAPIURL;
extern NSString * const EPAAccessToken;
extern NSString * const EPALogoutButtonTitle;
extern NSString * const EPAMediaTabTitle;
extern NSString * const EPAMediaIconTitle;
extern NSString * const EPASavedTabTitle;
extern NSString * const EPASavedIconTitle;
extern NSString * const EPAxcDataModelTitle;
extern NSString * const EPAExtencionForDataModel;
extern NSString * const EPADataBaseTitle;
extern NSString * const EPASortDescriptorKey;
extern NSString * const EPATagForInstagramSearch;
extern NSString * const EPACollectionCellNib;
extern NSString * const EPATableCellNib;

@end
