//
//  EPInstagramService.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EPDataProviderProtocol.h"
@interface EPAInstagramService : NSObject

@property (nonatomic,weak) id<EPADataProviderProtocol> delegate;

- (void)loadNextPageOfMedia;
- (NSInteger)getObjectsCount;
- (id)getObjectAtIndex:(NSInteger)index;

@end
