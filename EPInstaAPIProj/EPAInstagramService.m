//
//  EPInstagramService.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPAInstagramService.h"
#import <InstagramKit/InstagramKit.h>
#import "EPAMediaDataModel.h"
#import <Lockbox.h>
#import "EPAConstants.h"
@interface EPAInstagramService()

@property (nonatomic,strong) NSMutableArray *mediasArray;
@property (nonatomic,strong) NSString *maxId;

@end

@implementation EPAInstagramService

#pragma mark - init methods
- (instancetype)init {
    self = [super init];
    if (self) {
        self.mediasArray = [NSMutableArray new];
        [[InstagramEngine sharedEngine] setAccessToken:[Lockbox stringForKey:EPAInstagramApplicationAccessToken]];
        [self loadMedia];
    }
    return self;
}

#pragma mark - other methods
- (void)loadMedia {
    InstagramEngine *sharedEngine = [InstagramEngine sharedEngine];
    
    if (sharedEngine.accessToken){
        [self getTaggedMedia];
    }
}

- (void)getTaggedMedia {
    __weak typeof(self) weakSelf = self;
    [[InstagramEngine sharedEngine] getMediaWithTagName:EPATagForInstagramSearch withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        [weakSelf populateModelWithData:media];
        weakSelf.maxId = paginationInfo.nextMaxId;
        [weakSelf.delegate dataDidUpdated];
    } failure:^(NSError *error, NSInteger serverStatusCode) {
    }];
}

- (void)loadNextPageOfMedia {
    [self getTaggedMediaWithId:[self getNextMaxId]];
}

- (void)getTaggedMediaWithId:(NSString *)maxId {
    __weak typeof(self) weakSelf = self;
    [[InstagramEngine sharedEngine] getMediaWithTagName:EPATagForInstagramSearch count:20 maxId:maxId withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        [weakSelf populateModelWithData:media];
        weakSelf.maxId = paginationInfo.nextMaxId;
        [weakSelf.delegate dataDidUpdated];
    } failure:^(NSError *error, NSInteger serverStatusCode) {
    }];
}

- (void)populateModelWithData:(NSArray *)dataArray {
    for(InstagramMedia *mediaObj in dataArray) {
        EPAMediaDataModel *mediaDataModel = [[EPAMediaDataModel alloc] initWithInstagramMedia:mediaObj];
        [self.mediasArray addObject:mediaDataModel];
    }
}

- (NSString *)getNextMaxId {
    return self.maxId;
}

- (id)getObjectAtIndex:(NSInteger)index {
    return [self.mediasArray objectAtIndex:index];
}

- (NSInteger)getObjectsCount {
    return self.mediasArray.count;
}

@end
