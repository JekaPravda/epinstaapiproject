//
//  EPNavigationProtocol.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/11/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol EPANavigationProtocol <NSObject>

- (void)itemSelected:(id)media;

@end
