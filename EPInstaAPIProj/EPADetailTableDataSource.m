//
//  EPDetailTableDataSource.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPADetailTableDataSource.h"
#import "EPADetailCell.h"
#import "EPAMediaDataModel.h"
#import "EPASavedMedia.h"
#import "EPAConstants.h"
@interface EPADetailTableDataSource()

@property (nonatomic,strong)id media;

@end

@implementation EPADetailTableDataSource

#pragma mark - init methods
- (instancetype)initWithMedia:(id)media {
    self = [super init];
    if(self) {
        self.media = media;
    }
    
    return self;
}

#pragma mark - tableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EPADetailCell *cell = (EPADetailCell *)[tableView dequeueReusableCellWithIdentifier:EPADetailTableCellIdentifier];
    if ([self.media isKindOfClass:[EPASavedMedia class]]) {
        [cell populateWithSavedData:self.media];
    } else {
        [cell populateWithData:self.media];
    }
    return cell;
}

#pragma mark - tableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView bounds].size.height - 50;
}

@end
