//
//  EPASavedCollectionDataSource.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/6/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EPNavigationProtocol.h"
#import <CoreData/CoreData.h>
@interface EPASavedDataSource : NSObject <
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout,
    NSFetchedResultsControllerDelegate
>

@property (nonatomic,weak) UICollectionView *collectionView;
@property (nonatomic,weak) id<EPANavigationProtocol> delegate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
