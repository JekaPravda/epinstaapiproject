//
//  EPDetailCell.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPADetailCell.h"
#import <UIImageView+AFNetworking.h>
#import <MediaPlayer/MediaPlayer.h>
#import "EPACoreDataService.h"
#import "EPAConstants.h"
@interface EPADetailCell()

@property (nonatomic,strong) MPMoviePlayerViewController *controller;
@property (nonatomic,strong) EPAMediaDataModel *data;
@property (nonatomic,strong) EPASavedMedia *savedData;

@end

@implementation EPADetailCell

#pragma mark - init methods
- (void)populateWithData:(EPAMediaDataModel *)data{
    self.data = data;
    [self.saveButton setHidden:NO];
    if (data.videoIndicator) {
        [self playMovieWithURL:data.videoURL];
    } else {
        [self.image setImageWithURL:data.imageURL];
    }
    self.userName.text = data.userName;
    [self.userImage setImageWithURL:data.profileImageURL];
}

- (void)populateWithSavedData:(EPASavedMedia *)savedData {
    self.savedData = savedData;
    [self.deleteButton setHidden:NO];
    if ([savedData.videoIndicator  isEqual: @1]) {
        [self playMovieWithData:savedData.video];
    } else {
        [self.image setImage:[UIImage imageWithData:savedData.image]];
    }
    self.userName.text = savedData.userName;
    [self.userImage setImage:[UIImage imageWithData:savedData.profileImage]];
}
#pragma mark - others methods
- (void)playMovieWithURL:(NSURL *)videoURL {
    MPMoviePlayerViewController *movieController = [[MPMoviePlayerViewController alloc]
                                           initWithContentURL:videoURL];
    self.controller = movieController;
    movieController.view.frame = self.mediaView.bounds;
    [self.mediaView addSubview:movieController.view];
    [movieController.moviePlayer play];
}

- (void)playMovieWithData:(NSData *)videoData {
    NSString *filename = [NSHomeDirectory() stringByAppendingFormat:EPADocumentDirectory];
    NSData *filedata = [NSData dataWithData:videoData];
    NSError *error = nil;
    [filedata writeToFile:filename options:NSDataWritingAtomic error:&error];
    NSURL *fileURL = [NSURL fileURLWithPath:filename];
    [self playMovieWithURL:fileURL];
}

- (IBAction)saveButton:(id)sender {
    [[EPACoreDataService sharedService] saveMedia:self.data];
}

- (IBAction)deleteButton:(id)sender {
    [[EPACoreDataService sharedService] deleteMedia:self.savedData];
}

@end
