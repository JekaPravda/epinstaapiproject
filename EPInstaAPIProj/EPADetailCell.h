//
//  EPDetailCell.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPAMediaDataModel.h"
#import "EPNavigationProtocol.h"
#import "EPASavedMedia.h"
@interface EPADetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UIView *mediaView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

- (IBAction)saveButton:(id)sender;
- (IBAction)deleteButton:(id)sender;

- (void)populateWithData:(EPAMediaDataModel *)data;
- (void)populateWithSavedData:(EPASavedMedia *)savedData;

@end
