//
//  EPPopularDataSource.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/9/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EPDataProviderProtocol.h"
#import "EPNavigationProtocol.h"
@interface EPAPopularDataSource : NSObject <
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    EPADataProviderProtocol
>

@property (nonatomic,weak) id<EPANavigationProtocol> delegate;
@property (nonatomic,weak) UICollectionView *collectionView;

@end
