//
//  EPASavedCollectionDataSource.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/6/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPASavedDataSource.h"
#import "EPACollectionCell.h"
#import "EPASavedMedia.h"
#import "EPACoreDataService.h"
#import "EPAConstants.h"
@implementation EPASavedDataSource {
    NSMutableDictionary *_objectChanges;
}

#pragma mark - init methods
- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:EPASavedMediaEntityName inManagedObjectContext:[EPACoreDataService sharedService].managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:15];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:EPAKeyForDescriptor ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[EPACoreDataService sharedService].managedObjectContext sectionNameKeyPath:nil cacheName:EPAFetchedResultControllerCacheName];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    return _fetchedResultsController;
}

#pragma mark - collectionViewDataSource methods
- (EPACollectionCell *)collectionView:(UICollectionView *)collectionView
           cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    EPASavedMedia *savedMedia = [self.fetchedResultsController objectAtIndexPath:indexPath];
    EPACollectionCell *cell = (EPACollectionCell *)[collectionView    dequeueReusableCellWithReuseIdentifier:EPACollectionCellIdentifier forIndexPath:indexPath];
    [cell populateWithSavedData:savedMedia];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

#pragma mark - collectionViewDelegate methods
- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(EPACollectionCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    [cell clear];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate itemSelected:[self.fetchedResultsController objectAtIndexPath:indexPath]];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        return CGSizeMake(([[UIScreen mainScreen] bounds].size.width / 3)+5, ([[UIScreen mainScreen] bounds].size.width / 3)+5);
    }
    return CGSizeMake(([[UIScreen mainScreen] bounds].size.width / 3)-7, ([[UIScreen mainScreen] bounds].size.width / 3)-7);
}

#pragma mark - fetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    _objectChanges = [NSMutableDictionary dictionary];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    NSMutableArray *changeSet = _objectChanges[@(type)];
    if (changeSet == nil) {
        changeSet = [[NSMutableArray alloc] init];
        _objectChanges[@(type)] = changeSet;
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [changeSet addObject:newIndexPath];
            break;
        case NSFetchedResultsChangeDelete:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeUpdate:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [changeSet addObject:@[indexPath, newIndexPath]];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    NSMutableArray *moves = _objectChanges[@(NSFetchedResultsChangeMove)];
    if (moves.count > 0) {
        NSMutableArray *updatedMoves = [[NSMutableArray alloc] initWithCapacity:moves.count];
        if (updatedMoves.count > 0) {
            _objectChanges[@(NSFetchedResultsChangeMove)] = updatedMoves;
        } else {
            [_objectChanges removeObjectForKey:@(NSFetchedResultsChangeMove)];
        }
    }
    __weak typeof(self) weakSelf = self;
    [self.collectionView performBatchUpdates:^{
        NSArray *deletedItems = _objectChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedItems.count > 0) {
            [weakSelf.collectionView deleteItemsAtIndexPaths:deletedItems];
        }
        
        NSArray *insertedItems = _objectChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedItems.count > 0) {
            [weakSelf.collectionView insertItemsAtIndexPaths:insertedItems];
        }
        
        NSArray *reloadItems = _objectChanges[@(NSFetchedResultsChangeUpdate)];
        if (reloadItems.count > 0) {
            [weakSelf.collectionView reloadItemsAtIndexPaths:reloadItems];
        }
        
        NSArray *moveItems = _objectChanges[@(NSFetchedResultsChangeMove)];
        for (NSArray *paths in moveItems) {
            [weakSelf.collectionView moveItemAtIndexPath:paths[0] toIndexPath:paths[1]];
        }
    } completion:nil];
    _objectChanges = nil;
}

@end
