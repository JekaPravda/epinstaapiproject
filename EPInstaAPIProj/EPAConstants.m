//
//  EPAConstants.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/7/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPAConstants.h"
@implementation EPAConstants

NSString * const EPAInstagramApplicationAccessToken = @"appAccessToken";
NSString * const EPADocumentDirectory = @"/Documents/InstaVideo.mp4";
NSString * const EPACollectionCellIdentifier = @"CCell";
NSString * const EPASavedMediaEntityName = @"EPASavedMedia";
NSString * const EPAKeyForDescriptor = @"mediaId";
NSString * const EPAFetchedResultControllerCacheName = @"Root";
NSString * const EPADetailTableCellIdentifier = @"DCell";
NSString * const EPAWebPageForInternetCnnection = @"http://www.google.com";
NSString * const EPAInstagramAPIURL = @"%@?client_id=%@&redirect_uri=%@&response_type=token";
NSString * const EPAAccessToken = @"access_token=";
NSString * const EPALogoutButtonTitle = @"Logout";
NSString * const EPAMediaTabTitle = @"Media";
NSString * const EPAMediaIconTitle = @"media.png";
NSString * const EPASavedTabTitle = @"Saved";
NSString * const EPASavedIconTitle = @"saved.png";
NSString * const EPAxcDataModelTitle = @"InstagramMedia";
NSString * const EPAExtencionForDataModel = @"momd";
NSString * const EPADataBaseTitle = @"InstagramMedia.sqlite";
NSString * const EPASortDescriptorKey = @"mediaId";
NSString * const EPATagForInstagramSearch = @"nike";
NSString * const EPACollectionCellNib = @"EPACollectionCell";
NSString * const EPATableCellNib = @"EPADetailCell";

@end
