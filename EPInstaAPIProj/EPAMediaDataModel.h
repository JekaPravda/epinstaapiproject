//
//  EPMediaDataModel.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <InstagramKit/InstagramKit.h>
@interface EPAMediaDataModel : NSObject

- (instancetype)initWithInstagramMedia:(InstagramMedia *)mediaObject;

@property (nonatomic,assign) BOOL videoIndicator;
@property (nonatomic,strong) NSURL *videoURL;
@property (nonatomic,strong) NSURL *imageURL;
@property (nonatomic,strong) NSURL *thumbnailImageURL;
@property (nonatomic,strong) NSURL *profileImageURL;
@property (nonatomic,strong) NSString *userName;
@property (nonatomic,strong) NSString *mediaId;

@end
