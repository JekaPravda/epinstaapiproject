//
//  EPCoreDataService.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/30/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EPAMediaDataModel.h"
#import "EPASavedMedia.h"
@interface EPACoreDataService : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

+ (EPACoreDataService *)sharedService;
- (void)setupCoreData;
- (void)saveMedia:(EPAMediaDataModel *)media;
- (void)deleteMedia:(EPASavedMedia *)media;

@end
