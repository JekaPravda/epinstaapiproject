//
//  EPLauncher.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/30/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPALauncher.h"
#import "EPAAppDelegate.h"
#import <Lockbox/Lockbox.h>
#import <InstagramKit/InstagramKit.h>
#import "EPATabBarController.h"
#import "EPALoginViewController.h"
#import "EPACoreDataService.h"
#import "EPAConstants.h"
@implementation EPALauncher

#pragma mark - init methods
+ (void)launch {
    EPAAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    if ([Lockbox stringForKey:EPAInstagramApplicationAccessToken]) {
        [[InstagramEngine sharedEngine] setAccessToken:[Lockbox stringForKey:EPAInstagramApplicationAccessToken]];
        appDelegate.window.rootViewController = [EPATabBarController new];
    } else {
        appDelegate.window.rootViewController = [EPALoginViewController new];
    }
}

@end
