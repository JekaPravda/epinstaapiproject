//
//  EPPopularCell.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/9/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPASavedMedia.h"
#import "EPAMediaDataModel.h"

@interface EPACollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImage;
@property (weak, nonatomic) IBOutlet UIImageView *videoIndicator;

- (void)populateWithData:(EPAMediaDataModel *)data;
- (void)populateWithSavedData:(EPASavedMedia *)data;
- (void)clear;

@end
