//
//  EPASavedMediaViewController.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/6/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPASavedMediaViewController.h"
#import "EPASavedDataSource.h"
#import "EPADetailViewController.h"
#import "EPALoginViewController.h"
#import "EPAAppDelegate.h"
#import <Lockbox/Lockbox.h>
#import "EPAConstants.h"
@interface EPASavedMediaViewController ()

@property (nonatomic,strong) EPADetailViewController *detailViewController;
@property (nonatomic,strong) EPASavedDataSource *dataSource;

@end

@implementation EPASavedMediaViewController

#pragma mark - init methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.dataSource = [EPASavedDataSource new];
        self.dataSource.delegate = self;
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:EPASavedTabTitle image:[UIImage imageNamed:EPASavedIconTitle] tag:0];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:EPALogoutButtonTitle
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(logoutClicked:)];
    self.navigationItem.rightBarButtonItem = logoutButton;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:EPACollectionCellNib bundle:nil] forCellWithReuseIdentifier:EPACollectionCellIdentifier];
    self.dataSource.collectionView = self.collectionView;
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self.dataSource;
}

#pragma mark - other methods
- (IBAction)logoutClicked:(id)sender {
    [Lockbox setString:nil forKey:EPAInstagramApplicationAccessToken];
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    EPAAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.window.rootViewController = [EPALoginViewController new];
    
}

- (void)itemSelected:(id)media {
    [self.navigationController pushViewController:[[EPADetailViewController alloc] initWithMedia:media]animated:NO];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.collectionView performBatchUpdates:nil completion:nil];
}

@end
