//
//  main.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/8/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EPAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EPAAppDelegate class]));
    }
}
