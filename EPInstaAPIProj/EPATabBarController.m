//
//  EPATabBarController.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/2/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPATabBarController.h"
#import "EPAPopularViewController.h"
#import "EPASavedMediaViewController.h"
#import "EPAConstants.h"
@implementation EPATabBarController

#pragma mark - init methods
- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationController *popularNavController = [[UINavigationController alloc] initWithRootViewController:[EPAPopularViewController new]];
    UINavigationController *savedNavController = [[UINavigationController alloc] initWithRootViewController:[EPASavedMediaViewController new]];
    if ([self didConnected]) {
        self.viewControllers = @[popularNavController, savedNavController];
    } else {
        self.viewControllers = @[savedNavController, popularNavController];
    }
}

#pragma mark - other methods
- (BOOL)didConnected {
    NSURL *url=[NSURL URLWithString:EPAWebPageForInternetCnnection];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode]==200)?YES:NO;
}

@end
