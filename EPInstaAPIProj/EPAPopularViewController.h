//
//  EPPopularViewController.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/8/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPNavigationProtocol.h"
@interface EPAPopularViewController : UIViewController <EPANavigationProtocol>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
