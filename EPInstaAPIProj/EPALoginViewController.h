//
//  EPLoginViewController.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/11/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface EPALoginViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
