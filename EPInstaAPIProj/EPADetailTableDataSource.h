//
//  EPDetailTableDataSource.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface EPADetailTableDataSource : NSObject <
    UITableViewDataSource,
    UITableViewDelegate
>

- (instancetype)initWithMedia:(id)media;

@end
