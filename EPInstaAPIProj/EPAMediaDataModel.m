//
//  EPMediaDataModel.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPAMediaDataModel.h"
@implementation EPAMediaDataModel

#pragma mark - init methods
- (instancetype)initWithInstagramMedia:(InstagramMedia *)mediaObject {
    self = [super init];
    if(self){
        [self initModel:mediaObject];
    }
    return self;
}

- (void)initModel:(InstagramMedia *)mediaObject {
    self.mediaId = mediaObject.Id;
    self.videoIndicator = mediaObject.isVideo;
    self.videoURL = mediaObject.lowResolutionVideoURL;
    self.imageURL = mediaObject.standardResolutionImageURL;
    self.thumbnailImageURL = mediaObject.thumbnailURL;
    self.profileImageURL = mediaObject.user.profilePictureURL;
    self.userName = mediaObject.user.username;
}

@end
