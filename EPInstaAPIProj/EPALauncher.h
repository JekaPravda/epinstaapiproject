//
//  EPLauncher.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/30/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPALauncher : NSObject
+ (void)launch;
@end
