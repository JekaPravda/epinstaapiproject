//
//  SavedMedia.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/1/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@interface EPASavedMedia : NSManagedObject

@property (nonatomic, retain) NSNumber * videoIndicator;
@property (nonatomic, retain) NSString * mediaId;
@property (nonatomic, retain) NSData * video;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSData * thumbnailImage;
@property (nonatomic, retain) NSData * profileImage;
@property (nonatomic, retain) NSString * userName;

@end
