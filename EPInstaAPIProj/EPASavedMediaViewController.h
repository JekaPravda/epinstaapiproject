//
//  EPASavedMediaViewController.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/6/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPNavigationProtocol.h"
@interface EPASavedMediaViewController : UIViewController <EPANavigationProtocol>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
