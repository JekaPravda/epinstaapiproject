//
//  EPPopularCell.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/9/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPACollectionCell.h"
#import <UIImageView+AFNetworking.h>
@implementation EPACollectionCell

#pragma mark - init methods
- (void)populateWithData:(EPAMediaDataModel *)data {
    if (data.videoIndicator) {
        [self.videoIndicator setHidden:NO];
    }
    [self.thumbnailImage setImageWithURL:data.thumbnailImageURL];
}

- (void)populateWithSavedData:(EPASavedMedia *)data {
    if ([data.videoIndicator  isEqual: @1]) {
        [self.videoIndicator setHidden:NO];
    }
    [self.thumbnailImage setImage:[UIImage imageWithData:data.thumbnailImage]];
}

#pragma mark - init others
- (void)clear {
    [self.videoIndicator setHidden:YES];
    self.thumbnailImage.image = nil;
}

@end
