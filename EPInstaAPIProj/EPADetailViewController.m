//
//  EPDetailViewController.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPADetailViewController.h"
#import "EPADetailTableDataSource.h"
#import "EPAConstants.h"
@interface EPADetailViewController ()

@property (nonatomic,strong) EPADetailTableDataSource * dataSource;

@end

@implementation EPADetailViewController

#pragma mark - init methods
- (instancetype)initWithMedia:(id)media {
    self = [super init];
    if(self){
        self.dataSource = [[EPADetailTableDataSource alloc] initWithMedia:media];
        self.tableView.dataSource = self.dataSource;
        self.tableView.delegate = self.dataSource;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self.dataSource;
    self.tableView.delegate = self.dataSource;
    [self.tableView registerNib:[UINib nibWithNibName:EPATableCellNib bundle:nil] forCellReuseIdentifier:EPADetailTableCellIdentifier];
}

@end
