//
//  EPPopularDataSource.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/9/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPAPopularDataSource.h"
#import "EPACollectionCell.h"
#import "EPAMediaDataModel.h"
#import "EPAInstagramService.h"
#import "EPAConstants.h"
@interface EPAPopularDataSource()

@property (nonatomic,strong) EPAInstagramService *instagramService;

@end

@implementation EPAPopularDataSource

#pragma mark - init 
- (instancetype)init {
    self = [super init];
    if (self) {
        self.instagramService = [[EPAInstagramService alloc] init];
        self.instagramService.delegate = self;
    }
    return self;
}

#pragma mark - collectionViewDataSource methods
- (EPACollectionCell *)collectionView:(UICollectionView *)collectionView
          cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger lastCellIndex = [collectionView numberOfItemsInSection:0] - 1 ;
    if ((indexPath.row == lastCellIndex)) {
        [self getNextPageOfMedia];
    }
    EPACollectionCell *cell = (EPACollectionCell *)[collectionView    dequeueReusableCellWithReuseIdentifier:EPACollectionCellIdentifier forIndexPath:indexPath];
    [cell populateWithData:[self.instagramService getObjectAtIndex:indexPath.item]];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.instagramService.getObjectsCount;
}

#pragma mark - collectionViewDelegate methods
- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(EPACollectionCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell clear];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate itemSelected:[self.instagramService getObjectAtIndex:indexPath.item]];
}

#pragma mark - others
- (void)getNextPageOfMedia {
    [self.instagramService loadNextPageOfMedia];
}

- (void)dataDidUpdated {
    [self.collectionView reloadData];
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        return CGSizeMake(([[UIScreen mainScreen] bounds].size.width / 3)+5, ([[UIScreen mainScreen] bounds].size.width / 3)+5);
    }
    return CGSizeMake(([[UIScreen mainScreen] bounds].size.width / 3)-7, ([[UIScreen mainScreen] bounds].size.width / 3)-7);
}

@end
