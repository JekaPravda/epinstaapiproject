//
//  EPLoginViewController.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/11/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPALoginViewController.h"
#import <InstagramKit/InstagramKit.h>
#import <Lockbox/Lockbox.h>
#import "EPALauncher.h"
#import "EPAConstants.h"
@implementation EPALoginViewController

#pragma mark - init methods
- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *fullURL = [NSURL URLWithString:[NSString stringWithFormat:EPAInstagramAPIURL, [InstagramEngine sharedEngine].authorizationURL, [InstagramEngine sharedEngine].appClientID , [InstagramEngine sharedEngine].appRedirectURL]];
    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:fullURL]];
}

#pragma mark - webViewDelegate methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *URLString = [request.URL absoluteString];
    if ([URLString hasPrefix:[[InstagramEngine sharedEngine] appRedirectURL]]) {
        NSString *delimiter = EPAAccessToken;
        NSArray *components = [URLString componentsSeparatedByString:delimiter];
        if (components.count > 1) {
            NSString *accessToken = [components lastObject];
            [Lockbox setString:accessToken forKey:EPAInstagramApplicationAccessToken];
            [[InstagramEngine sharedEngine] setAccessToken:[Lockbox stringForKey:EPAInstagramApplicationAccessToken]];
            [EPALauncher launch];
            [self.webView removeFromSuperview];
        }
        return NO;
    }
    return YES;
}

@end
