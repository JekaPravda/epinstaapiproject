//
//  SavedMedia.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 7/1/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPASavedMedia.h"
@implementation EPASavedMedia

@dynamic videoIndicator;
@dynamic mediaId;
@dynamic video;
@dynamic image;
@dynamic thumbnailImage;
@dynamic profileImage;
@dynamic userName;

@end
