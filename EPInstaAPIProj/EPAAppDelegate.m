//
//  EPAppDelegate.m
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/8/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPAAppDelegate.h"
#import "EPAPopularViewController.h"
#import <Lockbox/Lockbox.h>
#import "EPALauncher.h"
#import "EPACoreDataService.h"
@implementation EPAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [[EPACoreDataService sharedService] setupCoreData];
    [EPALauncher launch];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}
@end
