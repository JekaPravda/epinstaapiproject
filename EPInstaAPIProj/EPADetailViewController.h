//
//  EPDetailViewController.h
//  EPInstaAPIProj
//
//  Created by Евгений Правда on 6/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface EPADetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (instancetype)initWithMedia:(id)media;

@end
